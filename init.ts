import { parse } from "https://deno.land/std@0.203.0/flags/mod.ts";
import { Untar } from "https://deno.land/std@0.203.0/archive/mod.ts";
import { readerFromStreamReader } from "https://deno.land/std@0.203.0/streams/mod.ts";
import * as Path from "https://deno.land/std@0.203.0/path/mod.ts";

const opts = parse(Deno.args, {
  string: ["in", "out"],
  default: {
    in:
      "https://gitlab.com/hesxenon/lectura/-/archive/main/lectura-main.tar.gz?path=template",
    out: `${Deno.cwd()}/docs`,
  },
});

Deno.mkdirSync(opts.out, { recursive: true });

fetch(opts.in)
  .then((res) => (res.body == null ? new ReadableStream() : res.body))
  .then((stream) => stream.pipeThrough(new DecompressionStream("gzip")))
  .then((stream) => readerFromStreamReader(stream.getReader()))
  .then((reader) => new Untar(reader))
  .then(async (untar) => {
    const directories = [] as string[];
    const entries = [] as Array<{
      filename: string;
      content: Uint8Array;
    }>;

    const rootRegex = /(.*)\/_config.ts/;
    let root_: string | undefined;

    for await (const entry of untar) {
      if (entry.fileName === "pax_global_header") {
        continue;
      }
      const filename = entry.fileName.replace(/.*?\//, "");
      if (filename === "") {
        continue;
      }
      if (entry.type === "directory") {
        directories.push(filename);
      } else {
        const matches = filename.match(rootRegex);
        if (matches?.[1] != null) {
          root_ = matches[1];
        }
        entries.push({
          filename,
          content: await (async () => {
            const chunk = new Uint8Array(entry.fileSize ?? 0);
            await entry.read(chunk);
            return chunk;
          })(),
        });
      }
    }

    if (root_ == null) {
      throw new Error(
        "could not determine root, make sure the specified archive has a _config.ts file in it",
      );
    }
    const root = root_;

    const sanitize = (path: string) =>
      Path.join(opts.out, path.replace(root, ""));

    return {
      directories: directories.map(sanitize),
      entries: entries.map((entry) => ({
        ...entry,
        filename: sanitize(entry.filename),
      })),
    };
  })
  .then(({ entries, directories }) =>
    Promise.all(
      directories.map((directory) =>
        Deno.mkdir(directory, { recursive: true })
      ),
    ).then(() => {
      const extensionsToPreserve = [".mdx", ".md", ".yml"];
      return Promise.all(
        entries.map(({ filename, content }) =>
          Deno.open(filename, {
            createNew: extensionsToPreserve.some((extension) =>
              filename.endsWith(extension)
            ),
            write: true,
          })
            .then((file) => file.write(content))
            .catch(() => undefined)
        ),
      );
    })
  );
