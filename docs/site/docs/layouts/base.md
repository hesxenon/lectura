---
order: 1
title: base.tsx
---

The base layout is used as... well, the base. It includes the header and the footer and everything in between is fair game.
