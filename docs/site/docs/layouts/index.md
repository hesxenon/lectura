---
order: 0
title: Layouts
---

Layouts in Lectura are used to embed your content and optionally enhance it.

You can use them by specifying them in the frontmatter with `layout: layouts/<name>`.
