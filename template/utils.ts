import * as React from "npm:preact";

export const isReactElement = React.isValidElement;

export const isFragment = (
  node: React.ComponentChild,
): node is Iterable<React.ComponentChild> =>
  node != null && typeof node === "object" && Symbol.iterator in node;

export const mapReactNode =
  (fn: (node: React.ComponentChild) => React.ComponentChild) =>
  (node: React.ComponentChild): React.ComponentChild => {
    const result = fn(
      isReactElement(node) && "children" in node.props
        ? {
            ...node,
            props: {
              ...node.props,
              children: mapReactNode(fn)(node.props.children),
            },
          }
        : isFragment(node)
        ? Array.from(node).map(mapReactNode(fn))
        : node,
    );
    return result;
  };

export const flatten = (node: React.ComponentChild): React.ComponentChild[] =>
  (node == null ? [] : isFragment(node) ? Array.from(node) : [node])
    .map((node) =>
      isReactElement(node) && "children" in node.props
        ? [node, ...flatten(node.props.children)]
        : [node],
    )
    .flat();

export const extract =
  <T extends React.ComponentChild>(
    predicate: (node: React.ComponentChild) => node is T,
  ) =>
  (node: React.ComponentChild) => {
    return flatten(node).filter(predicate);
  };
