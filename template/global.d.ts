import { AttributifyAttributes } from "https://esm.sh/@unocss/preset-attributify";

export {};

declare module "npm:preact" {
  namespace JSX {
    interface HTMLAttributes extends AttributifyAttributes {
      _?: string;
    }

    interface IntrinsicElements {
      template: HTMLAttributes<HTMLTemplateElement>;
    }
  }
}
