export default function Details({
  children,
  summary,
}: React.PropsWithChildren & { summary: React.ReactNode }) {
  return (
    <details className="group border rounded-lg border-light-contrast dark:border-dark-contrast mb-2 mt-2">
      <summary className="list-none flex group-open:rounded-b-0 rounded-lg items-center gap-2 p-2 hover:bg-light-contrast hover:dark:bg-dark-contrast group-open:bg-light-contrast-contrast dark:group-open:bg-dark-contrast">
        <div className="i-carbon-chevron-right text-xl transition group-open:rotate-90"></div>
        {summary}
      </summary>
      <article className="p-2">{children}</article>
    </details>
  );
}
