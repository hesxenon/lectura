import { Data, Page } from "lume/core.ts";
import * as Icons from "~/site/_components/icons.tsx";
import { Search } from "lume/plugins/search.ts";

export default function ({
  title,
  children,
  url,
  page,
  search,
}: React.PropsWithChildren & {
  title: string;
  url: string;
  page: Page;
  search: Search;
}) {
  const rootData = search.data("/");

  const headerMenuEntries = Array.from(
    search
      .files("**/*.html")
      .reduce((roots, url) => {
        const fragments = url.split("/").slice(1);
        if (fragments.length < 2 || fragments[0] == null) {
          return roots;
        }
        const root = fragments[0];
        const data = search.data(root) ?? {};
        const rootUrl = (data.url as string | false | undefined) ?? "/" + root;

        const page = search
          .pages(`url^=${rootUrl}`, "order=asc")
          .reduce((a, b) =>
            (a?.data.order ?? Number.MAX_VALUE) -
                (b?.data.order ?? Number.MAX_VALUE) ||
              (a?.data.url.length ?? Number.MAX_VALUE) -
                    (b?.data.url.length ?? Number.MAX_VALUE) <
                0
              ? a
              : b
          ) as Page | undefined;

        if (rootUrl === false || page == null) {
          return roots;
        }

        roots.set(rootUrl, { data, page });

        return roots;
      }, new Map<string, { data: Data; page: Page }>())
      .entries(),
  )
    .sort(
      ([, a], [, b]) => (a.data.order ?? Infinity) - (b.data.order ?? Infinity),
    )
    .map(([entryUrl, { data, page }]) => {
      return page.data.url === false ? undefined : (
        <a
          href={page.data.url}
          class={`p-4 inline-block hover:underline no-underline ${
            !url.includes(entryUrl)
              ? ""
              : "bg-light-contrast dark:bg-dark-contrast"
          }`}
        >
          {data.title ?? page.data.title ?? page.src.slug}
        </a>
      );
    });

  return (
    <html
      class="scroll-pt-20 scroll-smooth"
      _="
      on toggleDarkMode
        if I match .dark set nextscheme to 'light' else set nextscheme to 'dark' end
        send setColorScheme(colorscheme: nextscheme) to me
      end
      on setColorScheme(colorscheme) 
        add @disabled to <head link.code-theme/>

        if colorscheme is 'light' then 
          remove .dark from me 
          remove @disabled from <head link.code-theme.light/> 
        else 
          add .dark to me 
          remove @disabled from <head link.code-theme.dark/> 
        end

        call localStorage.setItem('color-scheme', colorscheme)
      end
      on load
        get matchMedia('(prefers-color-scheme: dark)').matches then put it into prefersDarkMode
        get localStorage.getItem('color-scheme') then put it into storedPreference
        set colorscheme to storedPreference or prefersDarkMode
        send setColorScheme(colorscheme: colorscheme) to me
      end
      "
    >
      <head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content={page.data.description} />
        <title>{title}</title>

        <script src="https://unpkg.com/hyperscript.org@0.9.11"></script>

        <link rel="stylesheet" href="/index.css" />

        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.8.0/styles/atom-one-dark.min.css"
          class="code-theme dark"
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.8.0/styles/atom-one-light.min.css"
          class="code-theme light"
        />

        <script
          type="module"
          dangerouslySetInnerHTML={{
            __html: `window.pagefind = await import("${
              page.data.root.slice(
                0,
                -1,
              )
            }/pagefind/pagefind.js"); pagefind.options({ baseUrl: "/" });`,
          }}
        >
        </script>
      </head>
      <body class="font-sans h-screen flex flex-col bg-light dark:bg-dark text-dark dark:text-light pt-14">
        <header class="z-50 h-14 flex border-b border-b-solid border-b-light-contrast dark:border-b-dark-contrast flex items-center gap-4 px-4 fixed w-full top-0 backdrop-blur-lg bg-light/30 dark:bg-dark/30">
          <a href="/" class="no-underline text-2xl font-bold">
            {rootData?.title ?? "Lectura"}
          </a>
          <div class="grow">{headerMenuEntries}</div>
          <search class="group relative">
            <Icons.Search class="absolute left-2 top-1/2 text-light-contrast dark:text-dark-contrast -translate-y-1/2 h-4" />
            <section class="peer hidden group-focus-within:group-[.has-results]:flex absolute top-full bg-light dark:bg-dark w-full border border-light-contrast dark:border-dark-contrast rounded-b-lg flex-col items-stretch max-h-screen/0.9 overflow-auto">
            </section>
            <input
              name="search"
              class="bg-transparent border border-light-contrast dark:border-dark-contrast p-2 pl-7 rounded-lg group-[.has-results]:focus:rounded-b-none outline-none w-100"
              placeholder="search..."
              _="
              on keydown[ctrlKey and key == 'k'] from <html/> halt the event then I.focus() end
              on keydown[key == 'Escape'] I.blur() end
              on input(target) debounced at 500ms 
                set results to previous <section/>
                set search to closest <search/>
                remove <*/> in results
                remove .has-results from search
                add .searching to search
                get pagefind.search(target.value) then put it into query
                remove .searching from search
                for result in query.results index i
                  add .has-results to search
                  get result.data() then put it into data
              log data
                  make an <a/> called hit
                  set hit.className to 'no-underline hover:bg-light-contrast dark:hover:bg-dark-contrast p-2'
                  set hit.href to data.url.replace('index.html', '')

                  make a <h4.font-bold/> called title
                  set title.innerText to data.meta.title
                  put title at the start of hit

                  make a <div.truncate/> called excerpt
                  set excerpt.innerHTML to data.excerpt
                  put excerpt at the end of hit

                  put hit at the end of results

                  if i < query.results.length - 1
                    make a <div.separator.border.border-light-contrast.dark:border-dark-contrast/> then put it at the end of results
                  end
                end
              end
              "
            />
          </search>
          <button _="
              on click send toggleDarkMode to closest <html/> end
              on mutation(target) of @class from closest <html/> 
                if it matches .dark 
                  remove .hidden from .moon in me
                  add .hidden to .sun in me
                else 
                  remove .hidden from .sun in me
                  add .hidden to .moon in me
                end
              end
              ">
            <Icons.Sun class="sun dark:hidden h-4" />
            <Icons.Moon class="moon hidden h-4" />
          </button>
        </header>
        <main class="gap-4 grow max-w-full prose prose-zinc dark:prose-invert">
          {children}
        </main>
        <footer class="z-50 bg-light dark:bg-dark border-t border-t-light-contrast dark:border-t-dark-contrast p-4">
          <a href="https//gitlab.com/hesxenon/lectura">repo</a>
        </footer>
      </body>
    </html>
  );
}
