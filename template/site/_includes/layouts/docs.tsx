import { Search } from "lume/plugins/search.ts";
import { Nav, NavData } from "lume/plugins/nav.ts";
import { extract, isReactElement } from "~/utils.ts";
import type { ComponentChild, ComponentChildren, JSX, VNode } from "npm:preact";
import icons from "~/site/_components/icons.tsx";

export const layout = "layouts/base.tsx";

const getWeight = ({ data, children }: NavData): number => {
  return children != null
    ? children.reduce(
        (lowest, { data: { order = Infinity } = {} }) =>
          lowest < order ? lowest : order,
        Infinity,
      )
    : data?.order ?? Infinity;
};

const sort = (a: NavData, b: NavData) => getWeight(a) - getWeight(b);

function Menu({ data, url }: { data: NavData; url: string }) {
  const href =
    data.data?.url == null ||
    data.data?.url === false ||
    typeof data.data?.url === "function"
      ? undefined
      : data.data?.url;

  const title = data.data?.title ?? data.slug;

  return (
    <details
      class={`p-2 ${data.children == null ? "" : "pl-7"} pr-0 group`}
      open={url.includes(data.slug)}
    >
      <summary
        class={`list-none cursor-pointer flex gap-1 items-center decoration-zinc-200 dark:decoration-zinc-600 decoration-2 underline-offset-4 ${
          url === data.data?.url ? "underline" : ""
        }`}
      >
        {data.children == null ? undefined : (
          <icons.ChevronRight class="group-open:rotate-90 transition" />
        )}
        {href == null ? (
          <span>{title}</span>
        ) : (
          <a class="no-underline" href={href}>
            {title}
          </a>
        )}
      </summary>
      {data.children
        ?.toSorted(sort)
        .map((child) => <Menu key={child.slug} data={child} url={url} />)}
    </details>
  );
}

function Sidebar({ children }: { children: ComponentChild }) {
  return (
    <>
      <details class="lg:hidden group :bg-light/30 open:dark:bg-dark/30 open:backdrop-blur-lg absolute top-0 first-of-type:left-0 last-of-type:right-0 w-14 min-w-min h-full">
        <summary class="list-none p-4 flex group-last-of-type:justify-end">
          <icons.Menu />
        </summary>
        <div class="w-56 pl-2">{children}</div>
      </details>
      <aside class="hidden group lg:block w-56 shrink-0 overflow-hidden">
        <div class="h-full p-4 group-first-of-type:mr-4 group-last-of-type:ml-4 shadow-2xl shadow-light-contrast dark:shadow-zinc-900">{children}</div>
      </aside>
    </>
  );
}

const isHeading = (
  node: ComponentChild,
): node is VNode<JSX.HTMLAttributes<HTMLHeadingElement>> =>
  isReactElement(node) &&
  typeof node.type === "string" &&
  ["h1", "h2", "h3", "h4", "h5", "h6"].includes(node.type);

const extractHeadings = extract(isHeading);

export default ({
  children,
  search,
  nav,
  url,
}: {
  children: ComponentChildren;
  title: string;
  search: Search;
  nav: Nav;
  url: string;
}) => {
  const root = "/" + url.split("/").slice(1)[0];

  const previous = search.previousPage(url, `url^=${root}`, "order=asc");
  const next = search.nextPage(url, `url^=${root}`, "order=asc");

  const headings = extractHeadings(children);

  return (
    <div class="flex h-full mx-auto relative container">
      <Sidebar>
        {nav
          .menu(root)!
          .children?.toSorted(sort)
          .map((data) => <Menu key={data.slug} data={data} url={url} />)}
      </Sidebar>
      <section class="p-4 px-14 lg:px-4 grow min-w-0">
        <article data-pagefind-body data-heading-anchors>
          {children}
        </article>

        <footer class="flex gap-4 pt-4">
          {previous == null ? undefined : (
            <a
              class="border border-light-contrast dark:border-dark-contrast rounded-lg p-4 flex gap-2 items-center grow cursor-pointer no-underline hover:bg-light-contrast dark:hover:bg-dark-contrast"
              href={previous.data.url}
              rel="previous"
            >
              <icons.ArrowLeft class="inline-block" />
              {previous.data.title ?? previous.data.url}
            </a>
          )}
          {next == null ? undefined : (
            <a
              class="border border-light-contrast dark:border-dark-contrast rounded-lg p-4 flex justify-end gap-2 items-center grow cursor-pointer no-underline hover:bg-light-contrast dark:hover:bg-dark-contrast"
              href={next.data.url}
              rel="next"
            >
              {next.data.title ?? next.data.url}
              <icons.ArrowRight class="inline-block" />
            </a>
          )}
        </footer>
      </section>
      <Sidebar>
        <div class="flex flex-col gap-2">
          {headings.map((node, i, { length }) => {
            const id = node.props.id;
            return (
              <>
                <a href={`#${id}`} class="no-underline">
                  {node.props.children}
                </a>
                {i >= length - 1 ? undefined : (
                  <div class="separator border border-light-contrast dark:border-dark-contrast"></div>
                )}
              </>
            );
          })}
        </div>
      </Sidebar>
    </div>
  );
};
