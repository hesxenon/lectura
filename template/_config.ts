import lume from "lume/mod.ts";
import jsx from "lume/plugins/jsx_preact.ts";
import mdx from "lume/plugins/mdx.ts";
import nav from "lume/plugins/nav.ts";
import pagefind from "lume/plugins/pagefind.ts";
import resolveUrls from "lume/plugins/resolve_urls.ts";
import basePath from "lume/plugins/base_path.ts";
import codeHighlight from "lume/plugins/code_highlight.ts";
import tailwindcss from "lume/plugins/tailwindcss.ts";
import colors from "npm:tailwindcss/colors.js";
import typography from "npm:@tailwindcss/typography";
import postcss from "lume/plugins/postcss.ts";
import remarkGfm from "npm:remark-gfm";
import rehypeSlug from "npm:rehype-slug";
import minifyHTML from "lume/plugins/minify_html.ts";

const site = lume({
  src: "./site",
  location: new URL("https://hesxenon.gitlab.io/lectura/"),
});

site.data("root", site.options.location.pathname);

site.preprocess([".md"], (page) => {
  page.data.templateEngine = "mdx";
});

site.process([".html"], function HeadingAnchors({ document, data }) {
  if (document == null) {
    return;
  }

  Array.from(
    document.querySelectorAll(
      "[data-heading-anchors] :is(h1, h2, h3, h4, h5, h6)[id]",
    ),
  ).forEach((node) => {
    const heading = node as unknown as Element;
    if (heading.textContent == null) {
      return;
    }
    const anchor = document.createElement("a");
    const id = heading.getAttribute("id")!;
    anchor.setAttribute("href", `${data.url}#${id}`);
    anchor.setAttribute("class", "no-underline");
    heading.setAttribute(
      "class",
      "flex gap-2 items-baseline after:text-light-contrast after:dark:text-dark-contrast after:content-['#'] after:invisible hover:after:visible",
    );
    heading.replaceWith(anchor as unknown as Node);
    anchor.appendChild(heading as unknown as typeof node);
  });
});

site.use(
  tailwindcss({
    extensions: [".html", ".tsx"],
    options: {
      darkMode: "class",
      theme: {
        extend: {
          colors: {
            light: colors.zinc[50],
            "light-contrast": colors.zinc[200],
            dark: colors.zinc[800],
            "dark-contrast": colors.zinc[700],
          },
        },
      },
      plugins: [typography],
    },
  }),
);
site.use(postcss());
site.use(jsx());
site.use(
  mdx({
    remarkPlugins: [remarkGfm],
    rehypePlugins: [rehypeSlug],
    overrideDefaultPlugins: false,
  }),
);
site.use(codeHighlight());
site.use(nav());
site.use(
  pagefind({
    ui: false,
  }),
);
site.use(basePath());
site.use(resolveUrls());
site.use(
  minifyHTML({
    extensions: [".html", ".css"],
  }),
);

export default site;
